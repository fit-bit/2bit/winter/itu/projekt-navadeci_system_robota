Projekt - Návrh jednoduchého řídícího systému robota
---------

**Popis:**
Vytvořte editor pro specifikaci prostoru, ve kterém se bude simulovat pohyb robota. Editor by měl umožňovat vytvářet libovolný prostor a vkládat do něj překážky v pohledu shora. Editor je možné rozšířit o další prvky, jako je specifikace počáteční polohy robota, jeho parametrů (rychlost apod.) tak, aby bylo možné simulaci spustit stiskem jediného tlačítka. Editor by měl umožňovat zobrazení cesty/cest robota do zvoleného bodu či průchod několika průběžnými body.

Body: 48/55